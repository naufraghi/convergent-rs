//! # Convergent Replicated Shopping list
//!
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, BufWriter};
use std::path::{Path, PathBuf};

use atomicwrites::{AllowOverwrite, AtomicFile};
use failure::Error;

use super::*;

/// The `Item` is the raw stored value in the list with some extra fields:
#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Hash, Clone)]
struct Item {
    /// The `SourceId` represents the device that generated the item
    source_id: SourceId,
    /// The `Stamp` is something like a Lamport timestamp (a counter + e real timestamp)
    stamp: Stamp,
    /// The real value we are storing in the shoppinglist
    value: String,
}

impl Progressive for Item {
    /// The `Progressive` implementation of item simply delegates to the `Stamp`
    fn current(&self) -> ProgressiveId {
        self.stamp.current()
    }
}

/// This is the real CRDT, we have 2 sets of items, one for the added values and one for the removed values:
/// - items are only added to one of the two sets, and never removed,
/// - to compute the resulting list of items, a live set difference is done.
///
/// This class is a low level storage that works with `Item`s, and not directly exposed to the user.
#[derive(Debug, Deserialize, Serialize, Default)]
struct InnerShoppingList {
    added: HashSet<Item>,
    removed: HashSet<Item>,
}

impl Progressive for InnerShoppingList {
    /// This implementation completes the Lamport timespamp, returning the max stamp for the current set of
    /// items.
    fn current(&self) -> ProgressiveId {
        let items = self.added.iter().chain(self.removed.iter());
        let progs = items.map(|i| i.current());
        progs.max().max(Some(ProgressiveId(0))).unwrap()
    }
}

impl InnerShoppingList {
    fn new() -> InnerShoppingList {
        InnerShoppingList::default()
    }
    fn add_item(&mut self, item: Item) {
        self.added.insert(item);
    }
    fn remove_item(&mut self, item: Item) {
        self.removed.insert(item);
    }
    fn items(&self) -> HashSet<&Item> {
        self.added.difference(&self.removed).collect()
    }
    fn grouped_by_name(&self) -> HashMap<&str, HashSet<&Item>> {
        let mut values = HashMap::new();
        for i in self.items() {
            values
                .entry(i.value.as_ref())
                .or_insert(HashSet::new())
                .insert(i);
        }
        values
    }
}

impl Merge for HashSet<Item> {
    fn merge(&self, other: &Self) -> Self {
        self.union(&other).cloned().collect()
    }
}

impl Merge for InnerShoppingList {
    fn merge(&self, other: &Self) -> Self {
        InnerShoppingList {
            added: self.added.merge(&other.added),
            removed: self.removed.merge(&other.removed),
        }
    }
}

/// A new shopping list is bound to a specific `SourceId`
pub trait NewShoppingList {
    fn new(source_id: SourceId) -> Self;
}

/// To be usable, a shopping list must implement this
pub trait UsableShoppingList {
    fn add(&mut self, item_name: &str) -> bool;
    fn del(&mut self, item_name: &str) -> bool;
    fn merge(&mut self, other: &Self);
    fn values(&self) -> Vec<&str>;
}

pub trait ReadWriteShoppingList: Sized {
    fn from_reader(read: impl Read) -> Result<Self, Error>;
    fn to_writer(&self, write: impl Write) -> Result<(), Error>;
}

// Waiting for https://github.com/rust-lang/rfcs/blob/master/text/1733-trait-alias.md
pub trait ShoppingList: NewShoppingList + UsableShoppingList + ReadWriteShoppingList {}

#[derive(Debug, Deserialize, Serialize)]
pub struct NamedShoppingList<S: Stamper = TimeStamper> {
    source_id: SourceId,
    list: InnerShoppingList,
    #[serde(skip_serializing)]
    stamper: S,
}

impl<S: Stamper> NewShoppingList for NamedShoppingList<S> {
    fn new(source_id: SourceId) -> NamedShoppingList<S> {
        NamedShoppingList {
            source_id,
            list: InnerShoppingList::new(),
            stamper: S::new(),
        }
    }
}
impl<S: Stamper> UsableShoppingList for NamedShoppingList<S> {
    /// Add a new value to the list. If no item with the same name is found, nothing is added, if no
    /// matching name is found, a new `Item` is created, with the current `source_id`, a new `stamp` and the
    /// provided `value`.
    fn add(&mut self, name: &str) -> bool {
        if !self.list.items().iter().any(|i| i.value == name) {
            let item = Item {
                source_id: self.source_id,
                stamp: self.stamper.stamp(&self.list),
                value: name.to_string(),
            };
            self.list.add_item(item);
            true
        } else {
            false
        }
    }

    /// Remove a value from the list, internally all the items with the matching value are added to the
    /// `removed` set (and therefore hidden from the summary list).
    ///
    /// Note: We could have used an LWW (Last-Write-Wins) structure but would have taken up slightly less
    /// space but at the cost of a slightly more complex extraction.
    fn del(&mut self, name: &str) -> bool {
        let to_be_removed: Vec<_> = self
            .list
            .items()
            .iter()
            .filter(|i| i.value == name)
            .map(|&i| i.clone())
            .collect();
        let mut removed = false;
        for item in to_be_removed {
            self.list.remove_item(item);
            removed = true;
        }
        removed
    }

    /// This `merge` operation is not the CRDT one, this time we are mutating the outer list storing the new
    /// version of the CRDT list.
    fn merge(&mut self, other: &Self) {
        self.list = self.list.merge(&other.list);
    }

    fn values(&self) -> Vec<&str> {
        self.list.grouped_by_name().keys().map(|k| *k).collect()
    }
}

// Waiting for https://github.com/rust-lang/rfcs/blob/master/text/1733-trait-alias.md
impl ShoppingList for NamedShoppingList {}

impl ReadWriteShoppingList for NamedShoppingList {
    fn from_reader(read: impl Read) -> Result<NamedShoppingList, Error> {
        let list = serde_json::from_reader(read)?;
        Ok(list)
    }

    fn to_writer(&self, write: impl Write) -> Result<(), Error> {
        serde_json::to_writer(write, &self)?;
        Ok(())
    }
}

pub struct FileShoppingList<T: ShoppingList> {
    pub list: T,
    filename: PathBuf,
}

/*
#[derive(Debug, Fail)]
enum FileShoppingListError {
    #[fail(display = "Unable to open file: {}", filename)]
    FileOpenError { filename: String },
    #[fail(display = "Unable to read from file: {}", filename)]
    FileReadError { filename: String },
    #[fail(display = "Unable to write to file: {}", filename)]
    FileWriteError { filename: String },
    #[fail(display = "Unable to deserialize json from: {:?}", data)]
    JsonDeserializeError { data: Vec<u8> },
    #[fail(display = "Unable to serialize to json: {:?}", data)]
    JsonSerializeError { data: NamedShoppingList },
}
*/

impl<T: ShoppingList> FileShoppingList<T> {
    pub fn new(source_id: SourceId, filename: &Path) -> Self {
        FileShoppingList {
            list: T::new(source_id),
            filename: filename.to_path_buf(),
        }
    }
    pub fn load(filename: &Path) -> Result<Self, Error> {
        let file = File::open(filename)?;
        let buf_reader = BufReader::new(file);

        Ok(FileShoppingList {
            list: T::from_reader(buf_reader)?,
            filename: filename.to_path_buf(),
        })
    }

    pub fn save(&self) -> Result<(), Error> {
        let af = AtomicFileWriter::new(&self.filename);
        let baf = BufWriter::new(af);
        self.list.to_writer(baf)
    }
}

struct AtomicFileWriter {
    af: AtomicFile,
}

impl AtomicFileWriter {
    fn new(path: &PathBuf) -> AtomicFileWriter {
        AtomicFileWriter {
            af: AtomicFile::new(path, AllowOverwrite),
        }
    }
}

use std::io;
impl Write for AtomicFileWriter {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.af.write(|f| f.write(buf)).map_err(|e| {
            io::Error::new(
                io::ErrorKind::Other,
                format!("AtomicFileWriter error: {}", e),
            )
        })
    }
    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl<T: ShoppingList> UsableShoppingList for FileShoppingList<T> {
    fn add(&mut self, item_name: &str) -> bool {
        self.list.add(item_name)
    }

    fn del(&mut self, item_name: &str) -> bool {
        self.list.del(item_name)
    }

    fn merge(&mut self, other: &Self) {
        self.list.merge(&other.list);
    }

    fn values(&self) -> Vec<&str> {
        self.list.values()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    struct DemoStamper {}
    impl Stamper for DemoStamper {
        fn new() -> DemoStamper {
            DemoStamper {}
        }
    }
    #[test]
    fn test_to_string() {
        let source_id = SourceId(0);
        let mut source_list: NamedShoppingList<DemoStamper> = NamedShoppingList::new(source_id);
        source_list.add("Value 1");
        source_list.add("Value 2");
        println!(
            "list = {}",
            serde_json::to_string(&source_list.list).unwrap()
        );
    }
    #[test]
    fn test_items() {
        let source_id = SourceId(0);
        let mut source_list: NamedShoppingList<DemoStamper> = NamedShoppingList::new(source_id);
        source_list.add("Value 1");
        source_list.add("Value 2");
        println!(
            "list = {}",
            serde_json::to_string(&source_list.list.items()).unwrap()
        );
        assert_eq!(source_list.list.items().len(), 2);
        let to_be_removed = source_list.list.items().into_iter().next().unwrap().clone();
        source_list.list.remove_item(to_be_removed);
        assert_eq!(source_list.list.items().len(), 1);
        println!(
            "list = {}",
            serde_json::to_string(&source_list.list.items()).unwrap()
        );
    }
    fn _merge_items() -> NamedShoppingList<DemoStamper> {
        let source_id_1 = SourceId(0);
        let mut source_list_1: NamedShoppingList<DemoStamper> = NamedShoppingList::new(source_id_1);
        source_list_1.add("Value 1");
        source_list_1.add("Value 2");
        assert_eq!(source_list_1.list.items().len(), 2);
        let source_id_2 = SourceId(0);
        let mut source_list_2: NamedShoppingList<DemoStamper> = NamedShoppingList::new(source_id_2);
        source_list_2.add("Value 2");
        source_list_2.add("Value 3");
        source_list_1.merge(&source_list_2);
        println!(
            "list = {}",
            serde_json::to_string(&source_list_1.list.items()).unwrap()
        );
        assert_eq!(source_list_1.list.grouped_by_name().len(), 3);
        println!("values = {:?}", source_list_1.list.grouped_by_name().keys());
        source_list_1
    }
    #[test]
    fn test_merge_items() {
        _merge_items();
    }
    #[test]
    fn test_remove_by_name() {
        let mut list = _merge_items();
        assert_eq!(list.list.items().len(), 4);
        assert_eq!(list.values().len(), 3);
        list.del("Value 2");
        assert_eq!(list.list.items().len(), 2);
        assert_eq!(list.values().len(), 2);
    }
    #[test]
    fn test_load_save_file() {
        let source_id = SourceId(0);
        let filename = PathBuf::from("test_load_save_file.json");
        let mut orig_list: FileShoppingList<NamedShoppingList> =
            FileShoppingList::new(source_id, &filename);
        orig_list.add("Value 1");
        orig_list.add("Value 2");
        orig_list.save().expect("Unable to save file");
        let loaded_list: FileShoppingList<NamedShoppingList> =
            FileShoppingList::load(&filename).expect("Unable to load file");
        assert_eq!(loaded_list.list.source_id, source_id);
        assert_eq!(loaded_list.values().len(), 2);
    }
}
