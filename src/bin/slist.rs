extern crate atomicwrites;
extern crate convergent;
#[macro_use]
extern crate failure;
extern crate serde_json;
extern crate structopt;

use failure::Error;
use std::path::PathBuf;
use structopt::StructOpt;

use convergent::shoppinglist::{FileShoppingList, NamedShoppingList, UsableShoppingList};
use convergent::SourceId;

#[derive(StructOpt, Debug)]
#[structopt(name = "slist")]
struct Opt {
    #[structopt(
        short = "s",
        long = "shoppinglist",
        default_value = "shoppinglist.json",
        env = "SLIST_SHOPPINGLIST_JSON",
        parse(from_os_str)
    )]
    /// Path of the shopping list file
    list: PathBuf,
    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(StructOpt, Debug)]
enum Command {
    #[structopt(name = "list")]
    /// Lists the items
    List,
    #[structopt(name = "add")]
    /// Adds an item to the list
    Add {
        /// Item to add
        item: String,
    },
    #[structopt(name = "del")]
    /// Removes an item from the list
    Del {
        /// Item to remove
        item: String,
    },
}

// The `SourceId` need to be external from the json list, and should be a one time configuration.
//
// TODO: decide if an opaque number is OK, or if it is better to have a multipart `SourceId` with a random
// part and a descriptive part. The descriptive part should be stored in a separate file or, if bundled, it
// should not be used to match the equality.
//
// The description map can be a bit asimmetric, the owner may like more details about the exact device used,
// instead another user may be interesed in the owner infos only, and not about the exact device.

fn main() -> Result<(), Error> {
    let opt = Opt::from_args();

    let mut list: FileShoppingList<NamedShoppingList> = if opt.list.exists() {
        FileShoppingList::load(&opt.list)?
    } else {
        let source_id = SourceId::new(0); // TODO; manage multiple `source_id`s
        FileShoppingList::new(source_id, &opt.list)
    };

    let res = match opt.cmd {
        Command::List => {
            println!("Items in the list:");
            for item in list.values() {
                println!("- {}", item);
            }
            Ok(())
        }
        Command::Add { item } => {
            if list.add(&item) {
                Ok(())
            } else {
                Err(format_err!("Item {} already present", item))
            }
        }
        Command::Del { item } => {
            if list.del(&item) {
                Ok(())
            } else {
                Err(format_err!("Item {} not present", item))
            }
        }
    };

    list.save()?;
    res
}
