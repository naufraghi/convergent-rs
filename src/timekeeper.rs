//! # Convergent Replicated Time keeper
//!
//! This module holds the basic types to store a time keeping log.
//!
//! La differenza tra la lista della spesa è il timekeeper è che la lista
//! è sempre coerente, non ha uno stato in cui è invalida.
//! Invece il timekeeper deve soddisfare alcuni vincoli di integrità, in particolare
//! due time-span non possono essere sovrapposti
//!
//! Quindi nella struttura ci possono archiviare tutti gli eventi di inizio e fine
//! attività, ma sarà necessaria una euristica per convertire una lista di eventi
//! in una registrazione di attività valida.
//!
//! Alcuni casi da gestire potrebbero essere:
//!
//! ### Caso 1
//!
//! 1. L'utente avvia una attività da connesso sul telefono alle 16:12
//! 2. Conclude l'attività sul portatile alle 18:26, ma in quel momento il wifi non prende.
//! 3. Arriva a casa e vede il timer che sta ancora contando sul telefono
//! 4. Lo blocca impostando come ora approssimativa le 18:30 dal telefono
//! 5. Più tardi riaccende il portatile e finalmente la conclusione dell'attività
//!    alle 18:26 arriva al server.
//!
//! L'utente si aspetta che la conclusione dell'attività sia quella effettiva,
//! ignorando l'inserimento approssimato.
//!
//! ### Caso 2
//!
//! 1. L'utente avvia una attività da disconnesso alle 9:01
//! 2. L'utente su un altro dispositivo connesso, nota che il timer non è partito
//!    e lo avvia di nuovo alle 9:05 (senza fare nessuna connessione)
//! 3. Il device disconnesso si connette ed invia inizio attività della 9:01
//!
//! L'utente si aspetta che l'attività cominci effettivamente alle 9:01
//!
//! ### Caso 3
//!
//! 1. L'utente avvia l'attività alle 11:34 da un dispositivo connesso
//! 2. L'utente modifica la descrizione dell'attività da un altro dispositivo
//!    alle 12:49
//! 3. L'utente interrompe l'attività sul primo dispositivo, prima che sia
//!    stata ricevuta la modifica alla descrizione alle 12:50
//!
//! L'utente si aspetta di veder comunque comparire la registrazione conclusa
//! alle 12:50 con la nuova descrizione.
//!
//! ## Possibile implementazione
//!
//! Potremmo registrare solo eventi di inizio attività, anche le pause sarebbero
//! un evento.
//! Ogni evento dovrebbe avere 2 timestamp, uno che marchi il momento in cui l'evento
//! è stato creato ed uno che marchi il momento in cui è cominciata l'attività che
//! stiamo misurando.
//!
//! > Un evento "misurato" sarebbe un evento dove il timestamp di inizio evento e quello
//! > di registrazione dell'evento coincidono. Si suppone che un evento misurato sia più
//! > accurato di un evento inserito in un altro momento.
//!
//! ```sh
//! (t_event, t_start, ...)
//! ```
//!
//! Per poter modificare un evento, dobbiamo trovare un modo per "migliorare" un evento
//! precedente, ma abbiamo già ipotizzato che l'evento `(event: 12:31, start: 12:31, desc: "prj1")` sia
//! migliore dell'evento `(event: 12:30, start: 13:01, event: "prj1 meeting")`.
//!
//! Ci sono 2 tipi di modifiche:
//!
//! 1. l'utente sta visualizzando un particolare evento e lo "modifica", quindi possiamo
//!    creare un evento che faccia riferimento al precedente,
//! 2. l'utente non ha ancora accesso all'evento e ne inserisce uno analogo, ma leggermente
//!    diverso. In questo caso serve una qualche euristica per decidere nel merge (nella
//!    sola visualizzazione) verrano scelte. Ad esempio il timestamp del primo evento,
//!    che supponiamo più accurato e la descrizione del secondo che supponiamo più ragionata.
//! 3. Se non fosse possibile unire i due eventi, la soluzione sempre possibile è quella di
//!    avere mostrare due eventi distinti.
//!
//! Questa terza opzione fa emerge la necessità di una terza feature, cioè quella di poter
//! creare un evento che risulti nell'unione di due registrazioni.
//!
//! In questo caso potremmo inserire gli eventi nella lista degli eventi cancellati in
//! modo simile a quello che facciamo nella lista della spesa. Si possono cancellare
//! solo eventi sincronizzati.
//!
use super::{SourceId, Stamp};

#[derive(Deserialize, Serialize, PartialEq, Eq, Hash, Clone)]
struct TimeSpan {
    source_id: SourceId,
    stamp: Stamp,
    value: String,
}
