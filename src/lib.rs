//! # Convergent Replicated Structures
//!
extern crate atomicwrites;
extern crate chrono;

extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
#[allow(unused_imports)]
#[macro_use] // seems to unlock the nested macro_use's
extern crate failure;

use chrono::prelude::*;
use serde::{Deserialize, Deserializer};

pub mod shoppinglist;
pub mod timekeeper;

/// The `SourceId` is a generic id that can be mapped to some specific info about the user, device, program
/// that originated the `Item`
#[derive(Debug, Deserialize, Serialize, Clone, Copy, PartialEq, Eq, Hash)]
pub struct SourceId(u64);

impl SourceId {
    pub fn new(id: u64) -> SourceId {
        SourceId(id)
    }
}

/// The `ProgressiveId` is a generic ordered id, somethink like a [Lamport
/// timestamp](https://en.wikipedia.org/wiki/Lamport_timestamps)
#[derive(Debug, Deserialize, Serialize, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ProgressiveId(u64);

impl ProgressiveId {
    fn increment(&self) -> ProgressiveId {
        ProgressiveId(self.0 + 1)
    }
}

/// `Stamp` is the auxiliar temporal identity of an item.
#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Hash, Clone)]
pub struct Stamp {
    date_created: DateTime<Utc>,
    progressive_id: ProgressiveId,
}

/// The trait `Progressive` is used to define an edit counter, the basic implementation of `successor()`
/// returns `current() + 1`
pub trait Progressive {
    fn current(&self) -> ProgressiveId;
    fn successor(&self) -> ProgressiveId {
        self.current().increment()
    }
}

pub trait Stamper {
    fn new() -> Self;
    fn now(&self) -> DateTime<Utc> {
        Utc::now()
    }
    fn stamp(&mut self, parent: &Progressive) -> Stamp {
        Stamp::new(parent, self.now())
    }
}

/// Default `Stamper` that returns `Utc::now()`
#[derive(Debug)]
pub struct TimeStamper;

impl Stamper for TimeStamper {
    fn new() -> Self {
        TimeStamper
    }
}

impl<'de> Deserialize<'de> for TimeStamper {
    fn deserialize<D>(_deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        Ok(Self::new())
    }
}

impl Stamp {
    /// This is the third part of the Lamport timestap, the `+ 1` step.
    fn new(parent: &Progressive, timestamp: DateTime<Utc>) -> Stamp {
        Stamp {
            date_created: timestamp,
            progressive_id: parent.successor(),
        }
    }
}

trait Merge {
    fn merge(&self, other: &Self) -> Self;
}

impl Progressive for Stamp {
    fn current(&self) -> ProgressiveId {
        self.progressive_id
    }
}
